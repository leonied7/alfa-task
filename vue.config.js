module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "~@/styles/common.scss";`,
      },
    },
  },
};
